package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService {

    @Autowired
    SoulRepository soulRespository;

    @Override
    public List<Soul> findAll() {
        return soulRespository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return soulRespository.findById(id);
    }

    @Override
    public void erase(Long id) {
        soulRespository.deleteById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        return soulRespository.save(soul);
    }

    @Override
    public Soul register(Soul soul) {
        return soulRespository.save(soul);
    }
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service

}

