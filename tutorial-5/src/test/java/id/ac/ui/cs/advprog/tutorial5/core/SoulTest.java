package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {
    Soul soul;

    @BeforeEach
    public void setUp() {
        soul = new Soul();
        soul.setAge(5);
        soul.setGender("M");
        soul.setName("Test");
        soul.setOccupation("Tester");
        soul.setId(1);
    }

    @Test
    public void testGetId() {
        assertEquals(1, soul.getId());
    }

    @Test
    public void testGetName() {
        assertEquals("Test", soul.getName());
    }

    @Test
    public void testGetGender() {
        assertEquals("M", soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        assertEquals("Tester", soul.getOccupation());
    }
}
