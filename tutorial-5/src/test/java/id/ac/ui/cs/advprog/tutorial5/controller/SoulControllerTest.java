package id.ac.ui.cs.advprog.tutorial5.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void whenAccessFindAll() throws Exception {
        mockMvc.perform(get("/soul"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testCreate() throws Exception {
        Soul soul = new Soul("Test", 5, "M", "Tester");
        soul.setId(1);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(soul);

        mockMvc.perform(post("/soul")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindByIdNotFound() throws Exception {
        mockMvc.perform(get("/soul/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdate() throws Exception {
        Soul soul = new Soul("Test", 5, "M", "Tester");
        soul.setId(1);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(soul);

        mockMvc.perform(put("/soul/1")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(delete("soul/1"))
                .andExpect(status().isNotFound());
    }
}
