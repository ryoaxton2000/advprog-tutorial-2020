package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImpTest {
    @Mock
    SoulRepository soulRepository;

    @InjectMocks
    SoulServiceImpl soulService;

    private Soul soul;

    @BeforeEach
    public void setUp() {
        soul = new Soul("Test", 5, "M", "Tester");
        soul.setId(1);
    }

    @Test
    public void testFindAll() {
        List<Soul> findSoul = soulService.findAll();
        lenient().when(soulService.findAll()).thenReturn(findSoul);
    }

    @Test
    public void testFindSoul() {
        Optional<Soul>  optionalSoul = soulService.findSoul(soul.getId());
        lenient().when(soulService.findSoul(soul.getId())).thenReturn(optionalSoul);
        lenient().when(soulService.findSoul((long)2)).thenReturn(null);
    }

    @Test
    public void testErase() {
        soulService.register(soul);
        soulService.erase((long)1);
        lenient().when(soulService.findSoul((long)1)).thenReturn(null);
    }

    @Test
    public void testRewrite() {
        Soul rewriter = soul;
        rewriter.setName("Rewriter");
        soulService.rewrite(rewriter);
        lenient().when(soulService.rewrite(rewriter)).thenReturn(rewriter);
    }

    @Test
    public void testRegister() {
        soulService.register(soul);
        lenient().when(soulService.register(soul)).thenReturn(soul);
    }
}
