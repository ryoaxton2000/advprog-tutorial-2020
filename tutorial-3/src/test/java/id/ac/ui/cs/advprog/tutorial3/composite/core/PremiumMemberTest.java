package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member test = new PremiumMember("Spotify", "Premium");
        member.addChildMember(test);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member test = new PremiumMember("Spotify", "Premium");
        member.addChildMember(test);
        member.removeChildMember(test);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new PremiumMember("Spotify", "Premium"));
        member.addChildMember(new PremiumMember("Youtube", "Premium"));
        member.addChildMember(new PremiumMember("Deezer", "Premium"));
        member.addChildMember(new PremiumMember("JOOX", "Premium"));
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Tidal", "Master");
        master.addChildMember(new PremiumMember("Spotify", "Premium"));
        master.addChildMember(new PremiumMember("Youtube", "Premium"));
        master.addChildMember(new PremiumMember("Deezer", "Premium"));
        master.addChildMember(new PremiumMember("JOOX", "Premium"));
        assertEquals(4, master.getChildMembers().size());
    }
}
