package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    int upVal;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.upVal = new Random().nextInt(5) + 1;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon == null) return upVal;
        return upVal + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Regular " + weapon.getDescription();
    }
}
