package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }
    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight.getArmor() != null && majesticKnight.getWeapon() != null);
        assertTrue(metalClusterKnight.getArmor() != null && metalClusterKnight.getSkill() != null);
        assertTrue(syntheticKnight.getSkill() != null && syntheticKnight.getWeapon() != null);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Drangleic Majestic Knight", majesticKnight.getName());
        assertEquals("Drangleic Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Drangleic Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertTrue(majesticKnight.getArmor().getDescription().equals("Metal Armor") && majesticKnight.getWeapon().getDescription().equals("Shining Buster"));
        assertTrue(metalClusterKnight.getArmor().getDescription().equals("Metal Armor") && metalClusterKnight.getSkill().getDescription().equals("Shining Force"));
        assertTrue(syntheticKnight.getSkill().getDescription().equals("Shining Force") && syntheticKnight.getWeapon().getDescription().equals("Shining Buster"));
    }

}
