package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new LordranAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight.getArmor() != null && majesticKnight.getWeapon() != null);
        assertTrue(metalClusterKnight.getArmor() != null && metalClusterKnight.getSkill() != null);
        assertTrue(syntheticKnight.getSkill() != null && syntheticKnight.getWeapon() != null);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight.setName("a");
        metalClusterKnight.setName("b");
        syntheticKnight.setName("c");
        assertEquals("a", majesticKnight.getName());
        assertEquals("b", metalClusterKnight.getName());
        assertEquals("c", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertTrue(majesticKnight.getArmor().getDescription().equals("Shining Armor") && majesticKnight.getWeapon().getDescription().equals("Thousand Jacker"));
        assertTrue(metalClusterKnight.getArmor().getDescription().equals("Shining Armor") && metalClusterKnight.getSkill().getDescription().equals("Thousand Years Of Pain"));
        assertTrue(syntheticKnight.getSkill().getDescription().equals("Thousand Years Of Pain") && syntheticKnight.getWeapon().getDescription().equals("Thousand Jacker"));
    }
}
